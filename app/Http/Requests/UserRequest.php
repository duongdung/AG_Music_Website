<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtUsername'=>'required|unique:users,name',
            'txtPass'=>'required|max:32|min:6',
            'txtResPass'=>'required|same:txtPass',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'rdoLevel' =>'required',
            'txtFullname'=>'required|min:3|max:15',
            'rdoLeve2'=>'required',
            'email'=>'required|email|unique:users,email',
            'txtPhone'=>'required|unique:users,email|regex:/(01)[0-9]{9}/',
        ];
    }
    public function messages(){
        return [
            'txtUsername.required'=>'Lỗi ! Bạn Chưa Nhập UserName.',
            'txtUsername.unique'=>'Lỗi ! UserName Đã Tồn Tại !',
            'txtPass.required'=>'Lỗi ! Bạn Chưa Nhập PassWord',
            'txtPass.max' => 'PassWord không vượt quá 32 ký tự',
            'txtPass.min'=>'PassWord không được ít hơn 6 ký tự',
            'txtResPass.required'=>'Lỗi ! Bạn chưa nhập lại PassWord',
            'txtResPass.same'=>'Lỗi ! PassWord nhập lại không khớp',
            'txtFullname.required'=>'Lỗi! Bạn Chưa Nhập Họ Tên',
            'txtFullname.min'=>'Lỗi ! Tên Phải Có Từ 3 Ký Tự Trở Lên',
            'txtFullname.max'=>'Lỗi ! Tên Không Được Quá 15 Ký Tự',
            'email.required'=>'Lỗi ! Bạn Chưa Nhập Email',
            'email.email' =>'Lỗi! Email không đúng định dạng',
            'email.unique'=>'Lỗi ! Email Đã Tồn Tại',
            'txtPhone.required'=>'Lỗi ! Bạn chưa Nhập Số Điện Thoại',
            'txtPhone.unique'=>'Lỗi ! SĐT đã tồn tại',
            'txtPhone.regex'=>'Lỗi ! SĐT không đúng định dạng VD :0123456789',
        ];
    }
}
