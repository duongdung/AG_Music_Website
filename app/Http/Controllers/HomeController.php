<?php

namespace App\Http\Controllers;

use App\Album;
use App\Singer;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection\Object;
use App\Song;
use App\Country;
use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function index()
    {
        $slideShow = $this->slideShow();
        $categoryList = $this->categoryList();
        $singerList = $this->singerList();
        return view('user.home', compact('slideShow', 'categoryList', 'singerList'));
    }

//    Lay danh sach nhac hot
    public function hotSongs()
    {
        $songs = Song::with('singer', 'author', 'album')
            ->where('ranking', '>', 0)
            ->orderBy('ranking')
            ->take(5)
            ->get();
        return response()->json(['playlist' => $songs]);
    }

//    Lay danh sach slideshow
    public function slideShow()
    {
        $slideShow = Album::where('hit', '1')
            ->take(6)
            ->get();
        return $slideShow;
    }

//    Lay danh sach the loai nhac theo quoc gia
    public function categoryList()
    {
        $categoryList = Country::with('categories')
            ->get();
        return $categoryList;
    }

//Lay danh sach ca sy
    private function singerList()
    {
        $singerList = Country::with('singers')
            ->get();
        return $singerList;
    }
}
