<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UserRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\User;
use Hash;
class UserController extends Controller
{
    public function getList(){
    	$user = User::all();
    	return view('admin.layout.user.list',['user'=>$user]);
    }
    public function getAdd(){
    	return view('admin.layout.user.add');
    }
    public function postAdd(UserRequest $req){
    	$user = new User();
    	$user->name = $req->txtUsername;
    	$user->password = Hash::make($req->txtPass);
    	$user->level = $req->rdoLevel;
    	$user->full_name = $req->txtFullname;
    	$user->gender = $req->rdoLeve2;
    	$user->email = $req->email;
    	$user->phone = $req->txtPhone;
    	$user->remember_token= $req->_token;

        if ($req->hasFile('image')) {//chek file
            $file = $req->file('image');  
            //thay ten random
            $name = $file->getClientOriginalName();
            $img = str_random(4).'_'.$name;
            while (file_exists("../resources/upload/".$img)) {
                $img = str_random(4).'_'.$name;
            }
            $file->move('../resources/upload',$img);
            $user->image = $img;
        }
    	$user->save();
    	return redirect()->route('admin.user.list')->with('thongbao','Thêm Thành Công');
    }
    public function getDelete($id){
        $user_login = Auth::user()->id;
        $user = User::find($id);
        if(($id ==1) || ($user_login !=2) && ($user["level"] == 1)) {
            return redirect()->route('admin.user.list')->with('error','Bạn Không Có Quyền Xóa Người Này !');
        }else{
            $user->delete($id);
            return redirect()->route('admin.user.list')->with('thongbao','Xóa Thành Công !');
        }
    }
    public function getEdit($id){
        $user_login = Auth::user()->id;
        $user = User::find($id);
        if ($id == 1 ||  $user_login !=2 && $user["level"] ==1 ) {
            return redirect()->route('admin.user.list')->with('error','Bạn Không Có Quyền Sửa Người Này !');
        }
        else{
            return view('admin.layout.user.edit',compact('user'));
        }
    }
    public function postEdit($id,Request $req){
         $user = User::find($id);
    }
    public function getLogin(){
        return view('admin.layout.login');
    }
    public function postLogin(LoginRequest $req){
        $login = [
            'name' => $req->name,
            'password' => $req->password,
            'level'=>1,
        ];
        if (Auth::attempt($login)) {
            return redirect()->route('admin.user.list');
        }
        else{ 
            return redirect()->route('admin.getLogin')->with('thongbao','Username hoặc Password không đúng !');
        }
    }
    public function getLogout(){
        Auth::logout();
        return redirect('admin/login');
    }
}
