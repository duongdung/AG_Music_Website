<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function categories(){
        return $this->hasMany('App\Category');
    }
    public function singers(){
        return $this->hasMany('App\Singer');
    }
}
