<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTable::class);
        $this->call(CountryTable::class);
        $this->call(CategoryTable::class);
        $this->call(SingerTable::class);
        $this->call(AuthorTable::class);
        $this->call(AlbumTable::class);
        $this->call(SongTable::class);
        $this->call(PlaylistTable::class);
        $this->call(CommentAlbumTable::class);
        $this->call(CommentSongTable::class);
    }
}
