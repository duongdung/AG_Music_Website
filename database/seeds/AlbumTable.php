<?php

use Illuminate\Database\Seeder;
use App\Album;
class AlbumTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $album = new Album();
        $album->name = "albummm";
        $album->alias = "album";
        $album->singer_id = 1;
        $album->country_id = 1;
        $album->category_id = 1;
        $album->img = "user/images/albums/album1.jpg";
        $album->number_view = 3;
        $album->user_id = 1;
        $album->number_song = 3;
        $album->hit = 1;
        $album->active = 1;
        $album->save();

        $album = new Album();
        $album->name = "albummm";
        $album->alias = "album";
        $album->singer_id = 3;
        $album->country_id = 2;
        $album->category_id = 2;
        $album->img = "user/images/albums/album2.jpg";
        $album->number_view = 3;
        $album->user_id = 1;
        $album->number_song = 3;
        $album->hit = 1;
        $album->active = 1;
        $album->save();

        $album = new Album();
        $album->name = "albummm";
        $album->alias = "album";
        $album->singer_id = 3;
        $album->country_id = 2;
        $album->category_id = 2;
        $album->img = "user/images/albums/album3.jpg";
        $album->number_view = 3;
        $album->user_id = 1;
        $album->number_song = 3;
        $album->hit = 1;
        $album->active = 1;
        $album->save();
    }
}
