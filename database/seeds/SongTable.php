<?php

use Illuminate\Database\Seeder;
use App\Song;
class SongTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $song = new Song();
        $song->name = "Chưa Bao Giờ Mẹ Kể";
        $song->alias = "chua-bao-gio-me-ke";
        $song->singer_id = 1;
        $song->author_id = 1;
        $song->album_id = 1;
        $song->category_id = 1;
        $song->user_id = 1;
        $song->url = "user/songs/Chua-Bao-Gio-Me-Ke-Ngay-Thu-8-Cua-Me-MIN-ERIK-Pham-Hoai-Nam.mp3";
        $song->image = "user/images/songs/chua-bao-gio-me-ke.jpg";
        $song->number_view = 10;
        $song->lyrics = "Chưa bao giờ mẹ kể";
        $song->active = 1;
        $song->bitrate = 128;
        $song->ranking = 1;
        $song->save();

        $song = new Song();
        $song->name = "Anh Thế Giới Và Em";
        $song->alias = "anh-the-gioi-va-em";
        $song->singer_id = 2;
        $song->author_id = 2;
        $song->album_id = 2;
        $song->category_id = 1;
        $song->user_id = 2;
        $song->url = "user/songs/Anh-The-Gioi-Va-Em-Huong-Tram.mp3";
        $song->image = "user/images/songs/anh-the-gioi-va-em.jpg";
        $song->number_view = 10;
        $song->lyrics = "No";
        $song->active = 1;
        $song->bitrate = 128;
        $song->ranking = 2;
        $song->save();

        $song = new Song();
        $song->name = "Có Em Chờ";
        $song->alias = "co-em-cho";
        $song->singer_id = 1;
        $song->author_id = 1;
        $song->album_id = 1;
        $song->category_id = 1;
        $song->user_id = 2;
        $song->url = "user/songs/Co-Em-Cho-Min-MrA.mp3";
        $song->image = "user/images/songs/chua-bao-gio-me-ke.jpg";
        $song->number_view = 10;
        $song->lyrics = "No";
        $song->active = 1;
        $song->bitrate = 128;
        $song->ranking = 3;
        $song->save();

        $song = new Song();
        $song->name = "Đưa Em Đi Khắp Thế Gian";
        $song->alias = "dua-em-di-khap-the-gian";
        $song->singer_id = 3;
        $song->author_id = 1;
        $song->album_id = 3;
        $song->category_id = 1;
        $song->user_id = 2;
        $song->url = "user/songs/Dua-Em-Di-Khap-The-Gian-Bich-Phuong.mp3";
        $song->image = "user/images/songs/chua-bao-gio-me-ke.jpg";
        $song->number_view = 10;
        $song->lyrics = "No";
        $song->active = 1;
        $song->bitrate = 128;
        $song->ranking = 4;
        $song->save();
    }
}
