<?php

use Illuminate\Database\Seeder;
use App\Singer;
class SingerTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singer = new Singer();
        $singer->name = "Bích Phương";
        $singer->alias = "bich-phuong";
        $singer->img = "bich-phuong.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Cẩm Ly";
        $singer->alias = "cam-ly";
        $singer->img = "cam-ly.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Đàm Vĩnh Hưng";
        $singer->alias = "dam-vinh-hung";
        $singer->img = "dam-vinh-hung.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Hà Anh Tuấn";
        $singer->alias = "ha-anh-tuan";
        $singer->img = "ha-anh-tuan.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Min";
        $singer->alias = "min";
        $singer->img = "min.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Mỹ Tâm";
        $singer->alias = "my-tam";
        $singer->img = "my-tam.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Soobin Hoàng Sơn";
        $singer->alias = "soobin-hoang-son";
        $singer->img = "soobin-hoang-son.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Thu Minh";
        $singer->alias = "thu-minh";
        $singer->img = "thu-minh.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Tuấn Hưng";
        $singer->alias = "tuan-hung";
        $singer->img = "tuan-hung.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Hồ Ngọc Hà";
        $singer->alias = "ho-ngoc-ha-";
        $singer->img = "ho-ngoc-ha-.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 1;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Adele";
        $singer->alias = "adele";
        $singer->img = "adele.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Alan Walker";
        $singer->alias = "alan-walker";
        $singer->img = "alan-walker.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Charlie Puth";
        $singer->alias = "charlie-puth";
        $singer->img = "charlie-puth.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Ed Sheeran";
        $singer->alias = "ed-sheeran";
        $singer->img = "ed-sheeran.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Enrique Lglesias";
        $singer->alias = "enrique-lglesias";
        $singer->img = "enrique-lglesias.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Justin bieber";
        $singer->alias = "justin-bieber";
        $singer->img = "justin-bieber.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Justin Timberlake";
        $singer->alias = "justin-timberlake";
        $singer->img = "justin-timberlake.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Maroon5";
        $singer->alias = "maroon5";
        $singer->img = "maroon5.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Shayne Ward";
        $singer->alias = "shayne-ward";
        $singer->img = "shayne-ward.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Rihana";
        $singer->alias = "rihana";
        $singer->img = "rihana.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 3;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "2NE1";
        $singer->alias = "2ne1";
        $singer->img = "2ne1.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Ailee";
        $singer->alias = "ailee";
        $singer->img = "ailee.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Beast";
        $singer->alias = "beast";
        $singer->img = "beast.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Bigbang";
        $singer->alias = "bigbang";
        $singer->img = "bigbang.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Boa";
        $singer->alias = "boa";
        $singer->img = "boa.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Davichi";
        $singer->alias = "davichi";
        $singer->img = "davichi.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "Exo";
        $singer->alias = "exo";
        $singer->img = "exo.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "IU";
        $singer->alias = "iu";
        $singer->img = "iu.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "jFla";
        $singer->alias = "jfla";
        $singer->img = "jfla.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1;
        $singer->save();

        $singer = new Singer();
        $singer->name = "T-ara";
        $singer->alias = "t-ara";
        $singer->img = "t-ara.jpg";
        $singer->introduction = "";
        $singer->number_song = 3;
        $singer->country_id = 2;
        $singer->hot = 1; 
        $singer->save();
    }
}
