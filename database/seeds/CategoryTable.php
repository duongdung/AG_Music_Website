<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoryTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//  Nhac viet Nam
        $category = new Category();
        $category->name = "Nhạc Trẻ";
        $category->alias = "nhac-tre";
        $category->image = "nhac-tre.png";
        $category->country_id = 1;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhạc Rock";
        $category->alias = "nhac-rock";
        $category->image = "tran-lap.jpg";
        $category->country_id = 1;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhạc Trữ Tình";
        $category->alias = "nhac-tru-tinh";
        $category->image = "cam-ly.jpg";
        $category->country_id = 1;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhạc Trịnh";
        $category->alias = "nhac-trinh";
        $category->image = "trinh-cong-son.jpg";
        $category->country_id = 1;
        $category->number_song = 2;
        $category->save();

//  Nhac Chau A
        $category = new Category();
        $category->name = "Hàn Quốc";
        $category->alias = "nhac-han-quoc";
        $category->image = "boa.jpg";
        $category->country_id = 2;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Hoa Ngữ";
        $category->alias = "nhac-hoa-ngu";
        $category->image = "jay-chou.jpg";
        $category->country_id = 2;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhật Bản";
        $category->alias = "nhac-nhat-ban";
        $category->image = "nhat-ban.jpg";
        $category->country_id = 2;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Thái Lan";
        $category->alias = "nhac-thai-lan";
        $category->image = "thai-lan.jpg";
        $category->country_id = 2;
        $category->number_song = 2;
        $category->save();

//Nhac au my
        $category = new Category();
        $category->name = "Nhạc Pop";
        $category->alias = "nhac-pop";
        $category->image = "charlie-puth.jpg";
        $category->country_id = 3;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhạc Rok";
        $category->alias = "nhac-rock";
        $category->image = "likin-park.jpg";
        $category->country_id = 3;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhạc Rap/Hiphop";
        $category->alias = "nhac-rap";
        $category->image = "eminem.jpg";
        $category->country_id = 3;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhạc Dance";
        $category->alias = "nhac-dance";
        $category->image = "alan-walker.jpg";
        $category->country_id = 3;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhạc R&B";
        $category->alias = "nhac-rb";
        $category->image = "adele.jpg";
        $category->country_id = 3;
        $category->number_song = 2;
        $category->save();

        $category = new Category();
        $category->name = "Nhạc AudioPhile";
        $category->alias = "nhac-audiophile";
        $category->image = "audiophile.jpg";
        $category->country_id = 3;
        $category->number_song = 2;
        $category->save();
    }
}
