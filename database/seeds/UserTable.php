<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "duongdung";
        $user->password = bcrypt("A12345");
        $user->image = "user/images/imguser/user1.png";
        $user->level = 1;
        $user->full_name = "Duong Dung";
        $user->gender = "Nam";
        $user->email = "dung@gmail.com";
        $user->phone = "113";
        $user->save();

        $user = new User();
        $user->name = "phanluan";
        $user->password = bcrypt("A12345");
        $user->image = "user/images/imguser/user1.png";
        $user->level = 1;
        $user->full_name = "Phan Luan";
        $user->gender = "Nữ";
        $user->email = "luan@gmail.com";
        $user->phone = "113";
        $user->save();
    }
}
