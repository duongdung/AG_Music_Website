<?php

use Illuminate\Database\Seeder;
use App\Country;
class CountryTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = new Country();
        $country->name = "Việt Nam";
        $country->save();

        $country = new Country();
        $country->name ="Châu Á";
        $country->save();

        $country = new Country();
        $country->name = "Âu Mỹ";
        $country->save();
    }
}
