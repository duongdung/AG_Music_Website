<?php

use Illuminate\Database\Seeder;
use App\Playlist;
class PlaylistTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $playList = new Playlist();
        $playList->name = "list 1";
        $playList->user_id = 1;
        $playList->song_url = "1:2:3";
        $playList->save();

        $playList = new Playlist();
        $playList->name = "list 2";
        $playList->user_id = 1;
        $playList->song_url = "2:3:4";
        $playList->save();
    }
}
