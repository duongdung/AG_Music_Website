<?php

use Illuminate\Database\Seeder;
use App\CommentSong;
class CommentSongTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comment = new CommentSong();
        $comment->user_id = 1;
        $comment->song_id = 1;
        $comment->content = "123123123";
        $comment->save();

        $comment = new CommentSong();
        $comment->user_id = 2;
        $comment->song_id = 2;
        $comment->content = "123123123";
        $comment->save();
    }
}
