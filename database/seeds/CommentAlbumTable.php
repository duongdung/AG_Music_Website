<?php

use Illuminate\Database\Seeder;
use App\CommentAlbum;
class CommentAlbumTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comment = new CommentAlbum();
        $comment->user_id = 1;
        $comment->album_id = 1;
        $comment->content = "123123123";
        $comment->save();

        $comment = new CommentAlbum();
        $comment->user_id = 2;
        $comment->album_id = 2;
        $comment->content = "123123123";
        $comment->save();
    }
}
