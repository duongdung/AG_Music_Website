<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('admin/login',['as'=>'admin.getLogin','uses'=>'UserController@getLogin']);

Route::post('admin/login',['as'=>'admin.postLogin','uses'=>'UserController@postLogin']);

Route::get('admin/logout',['as'=>'admin.getLogout','uses'=>'UserController@getLogout']);

Route::group(['prefix'=>'admin'],function(){
	Route::group(['prefix'=>'user'],function(){
		Route::get('list',['as'=>'admin.user.list','uses'=>'UserController@getList']);
		Route::get('add',['as'=>'admin.user.add','uses'=>'UserController@getAdd']);
		Route::post('add',['as'=>'admin.user.postadd','uses'=>'UserController@postAdd']);
		Route::get('edit/{id}',['as'=>'admin.user.getEdit','uses'=>'UserController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.user.postEdit','uses'=>'UserController@postEdit']);
		Route::get('delete/{id}',['as'=>'admin.user.getDelete','uses'=>'UserController@getDelete']);
	});
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/hot-songs', 'HomeController@hotSongs');
