(function () {
    $.get({
        url: '/hot-songs',
        success: function (response) {
            var playlist = [];
            response.playlist.forEach(function (song) {
                // if (song.ranking > 0) {
                    playlist.push({
                        file: "storage/upload/songs/"+song.url,
                        thumb: "storage/upload/images/songs/"+song.image,
                        trackName: song.name,
                        trackArtist: song.singer.name,
                        trackAuthor: song.author.name,
                        trackAlbum: song.album.name
                    });
                // }
            });
            $(".jAudio").jAudio({playlist: playlist});
        }
    });
})();