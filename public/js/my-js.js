/**
 * Created by duongdung on 7/5/17.
 */
$(document).ready(function () {
    var backgroundHead = ".quotes-bg";
    smoothPage();

    changeButtonMenu();

    changeButtonPlay();

    $(window).bind('scroll', function () {
        parallax(backgroundHead);
    });

    $('.btn-volume').click(changeVolume);

    getPlaylist();

    $('.button-list').click(hidePlaylist);

    progressBarAudio();

});
// Thay doi icon button menu
function changeButtonMenu() {
    var flag = true;
    var btnMenu = $('.button-menu');
    var iconMenu = $('.icon-menu');
    var mainMenu = $('.main-nav');

    btnMenu.click(function (event) {
        if (flag) {
            mainMenu.css('background-color', '#fff');
            iconMenu.removeClass('fa-bars');
            iconMenu.addClass('fa-times');
            flag = false;
        } else {

            iconMenu.removeClass('fa-times');
            iconMenu.addClass('fa-bars');
            mainMenu.css('background-color', 'transparent');
            flag = true;
        }
    })
}

// Hiệu ứng parallax
function parallax(className) {
    var scrollPoint = $(window).scrollTop();
    var speed = 0.23;
    $(className).css('top', (0 - (scrollPoint * speed)) + 'px');
}

// Hieu ung cuon trang
function smoothPage() {

    var $window = $(window);
    var scrollTime = 1.2;
    var scrollDistance = 170;

    $window.on("mousewheel DOMMouseScroll", function (event) {

        event.preventDefault();

        var delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
        var scrollTop = $window.scrollTop();
        var finalScroll = scrollTop - parseInt(delta * scrollDistance);

        TweenMax.to($window, scrollTime, {
            scrollTo: {y: finalScroll, autoKill: true},
            ease: Power1.easeOut,
            overwrite: 5
        });

    });
}

// hieu ung muted
function changeVolume() {
    var muted = myAudio.muted;
    // console.log(muted);
    if (muted !== true) {
        myAudio.muted = true;
        $('.volume-icon').addClass('fa-volume-off');
        $('.volume-icon').removeClass('fa-volume-up');
    } else {
        myAudio.muted = false;
        $('.volume-icon').addClass('fa-volume-up');
        $('.volume-icon').removeClass('fa-volume-off');
    }
}

// Hieu ung hien playlist
function hidePlaylist() {
    var list = $('.jAudio--playlist').css('left');
    if (list === '0px') {
        $('.jAudio--playlist').css('left', '-100%');
        $('.button-list').css({'top': '35%', 'color': 'white', 'border': '1px solid white'});
        $('.player-controls').css('background-color','transparent');
    } else {
        $('.jAudio--playlist').css('left', '0');
        $('.button-list').css({'top': '80%', 'color': '#FF3327', 'border': '1px solid #FF3327'});
        $('.player-controls').css('background-color','white');
    }
}

// Thay doi nut play tren player
function changeButtonPlay() {
    var icon = $('.arrow-play');
    var flag = true;

    $('.play-pause').click(function (event) {
        if (flag) {
            icon.addClass('fa-pause');
            icon.removeClass('fa-play');
            $('.music-card').removeClass('playing');
            flag = false;
        } else {
            icon.addClass('fa-play');
            icon.removeClass('fa-pause');
            $('.music-card').addClass('playing');
            flag = true;
        }
    })
}

// Lay time gan len thanh progress cua player
function progressBarAudio() {
    var player = document.getElementById('myAudio');
    player.addEventListener("timeupdate", function () {

        var currentTime = player.currentTime;
        var minutescurrent = Math.floor(currentTime / 60);
        var secondscurrent = (currentTime % 60).toFixed(0);

        var duration = player.duration;
        var minutesDuration = Math.floor(duration / 60);
        var secondsDuration = (duration % 60).toFixed(0);

        $('.progress-span').stop(true, true).animate({'width': (currentTime + .25) / duration * 100 + '%'}, 250, 'linear');
        $('.duration').html(minutesDuration + ':' + secondsDuration);
        $('.current-time').html(minutescurrent + ':' + secondscurrent);
    });
}

// Lay danh sach bai nhac len player
function getPlaylist() {
    $(".jAudio--player").jAudio({
        playlist: [],
        defaultAlbum: undefined,
        defaultArtist: undefined,
        defaultTrack: 0,
        autoPlay: false,
        debug: false
    });
}