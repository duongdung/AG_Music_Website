<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AG_Music MP3</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- MY CSS -->
    <link  href="{!!asset('admin/css/style.css') !!}" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <div class="overlay"></div>
        <div class="row">
            <div class="col-md-12">
              @include('admin.layout.header')
            </div>
        </div>
        <!-- Sidebar -->
            @include('admin.layout.sidebar')
        <!-- /#sidebar-wrapper -->
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
                <span class="hamb-middle"></span>
                <span class="hamb-bottom"></span>
            </button>

            <div class="container">
                <div class="row">

                    <div class="col-md-12 box-container">
                        <div class="panel panel-default">
                            <div class="col-lg-12">
                            @if(session('thongbao'))
                                     <div class="alert alert-success">
                                        {{session('thongbao')}}
                                     </div>
         
                            @endif
                            </div>
                                @yield('content')
                        </div>   
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
     <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- nav.js -->
    <script src="{!! asset('admin/js/nav.js')!!}"></script>
    @yield('script')
</body>
</html>