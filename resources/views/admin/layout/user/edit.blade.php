@extends('admin.master')
@section('content')
	<div class="panel-heading"><h3><span class="glyphicon glyphicon-user"></span>Edit User</h3></div>
<form role="form" method="POST">
	<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
	@include('admin.layout.block.error')
	<div class="form-group">
	    <label>User_Name</label>
	    <input type="text" class="form-control" name="txtUsername" placeholder="Enter username" value="{!! $user['username']!!}">
  	<div class="form-group">
            <label>Level</label>
            <label class="radio-inline">
                <input name="rdoLevel" value="1" type="radio"
                @if($user["level"]==1)
	            checked = "checked"
	            @endif
	           	>Admin
            </label>
            <label class="radio-inline">
                <input name="rdoLevel" value="2" type="radio"
                @if($user["level"]==2)
	            checked = "checked"
	            
	            @endif
	            >Member
            </label>
    </div>
    <div class="form-group">
	    <label for="exampleInputEmail1">Full Name</label>
	    <input type="text" class="form-control" name="txtFullname" placeholder="Enter Full Name" value="{!! $user['full_name']!!}">
  	</div>
	<div class="form-group">
            <label>Gender</label>
            <label class="radio-inline">
                <input name="rdoLeve2" value="0" type="radio"
                @if($user["gender"]==0)
                checked = "checked"
            
                @endif
                >Nam
            </label>
            <label class="radio-inline">
                <input name="rdoLeve2" value="1" type="radio"
                @if($user["gender"]==1)
	            checked = "checked"
	            
	            @endif
	            >Nữ
            </label>
    </div>
	<div class="form-group">
		<label>Email address</label>
		<input type="email" class="form-control" name="email" placeholder="Enter email" value="{!! $user['email']!!}">
	</div>
	<div class="form-group">
	    <label>Phone</label>
	    <input type="text" class="form-control" name="txtPhone" placeholder="Enter Phone Number" value="{!! $user['phone']!!}">
  	</div>
	<button type="submit" class="btn btn-default">Edit</button>
	<button type="reset" class="btn btn-default">Reset</button>
</form>
@endsection
