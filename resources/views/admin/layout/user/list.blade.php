@extends('admin.master')
@section('content')
<div class="panel-heading">
  <div class="col-md-12">
        @if(session('error'))
                 <div class="alert alert-danger">
                    {{session('error')}}
                 </div>

        @endif
  </div>
  <div class="user-icon">
    <h3><span class="glyphicon glyphicon-user"></span>User</h3>
  </div>
  <a href="{{ route('admin.user.postadd')}}"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span>Add New</button></a>
  </div>
<table class="table table-bordered table-responsive">
  <thead>
    <tr>
      <th>STT</th>
      <th>Tài Khoản</th>
      <th>Level</th>
      <th>Họ Tên</th>
      <th>Giới Tính</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Hành Động</th>
    </tr>
  </thead>
  <tbody>
    <?php $stt = 0 ?>
    @foreach($user as $item)
    <?php $stt = $stt + 1 ?>
    <tr>
      <td>{!! $stt !!}</td>
      <td>{!! $item['name']!!}</td>
      <td>
        @if($item["id"] == 2)
        SuperAdmin
        @elseif($item["level"] == 1)
        Admin
        @else
        Member
        @endif
      </td>
      <td>{!! $item['full_name']!!}</td>
      <td>
        @if($item["gender"]== 0)
        Nam
        @else
        Nữ
        @endif
      </td>
      <td>{!! $item['email']!!}</td>
      <td>{!! $item['phone']!!}</td>
      <td>

        <a href="{{ URL::route('admin.user.getEdit',$item['id']) }}"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></a>

        <a href="{{ URL::route('admin.user.getDelete',$item['id']) }}" onclick="return xacnhanxoa('Bạn có thực sự muốn xóa người này!')"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span>Delete</button></a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<script type="text/javascript">
  function xacnhanxoa(msg) {
    if (window.confirm(msg)) {
      return true;
    }
    return false;
  }
</script>
@endsection