@extends('admin.master')
@section('content')
<div class="panel-heading"><h3><span class="glyphicon glyphicon-user"></span>New User</h3></div>
<form role="form" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
	@include('admin.layout.block.error')
	<div class="form-group">
	    <label>User_Name</label>
	    <input type="text" class="form-control" name="txtUsername" placeholder="Enter username" value="{!! old('txtUsername') !!}">
  	</div>
  		<div class="form-group">
		<label>Password</label>
		<input type="password" class="form-control" name="txtPass"  placeholder="Password" >
	</div>
		<div class="form-group">
		<label>ResPassword</label>
		<input type="password" class="form-control" name="txtResPass"  placeholder=" ResPassword">
	</div>
	<div class="form-group">
	    <label>Image</label>
	    <input type="file" class="form-control" name="image">
  	</div>
  	<div class="form-group">
            <label>Level</label>
            <label class="radio-inline">
                <input name="rdoLevel" value="1" checked="" type="radio">Admin
            </label>
            <label class="radio-inline">
                <input name="rdoLevel" value="2" type="radio">Member
            </label>
    </div>
    <div class="form-group">
	    <label for="exampleInputEmail1">Full Name</label>
	    <input type="text" class="form-control" name="txtFullname" placeholder="Enter Full Name">
  	</div>
	<div class="form-group">
            <label>Gender</label>
            <label class="radio-inline">
                <input name="rdoLeve2" value="Nam" checked="" type="radio">Nam
            </label>
            <label class="radio-inline">
                <input name="rdoLeve2" value="Nữ" type="radio">Nữ
            </label>
    </div>
	<div class="form-group">
		<label>Email address</label>
		<input type="email" class="form-control" name="email" placeholder="Enter email" value="{!! old('email')!!}">
	</div>
	<div class="form-group">
	    <label>Phone</label>
	    <input type="text" class="form-control" name="txtPhone" placeholder="Enter Phone Number" value="{!! old('txtPhone')!!}">
  	</div>
	<button type="submit" class="btn btn-default">Submit</button>
	<button type="reset" class="btn btn-default">Reset</button>
</form>
@endsection