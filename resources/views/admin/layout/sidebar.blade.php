<nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
    <ul class="nav sidebar-nav">
        <li class="sidebar-brand">
            <a href="#"><span class="glyphicon glyphicon-dashboard"></span>
                Dashboard
            </a>
        </li>
        <li>
            <a href="#"><span class="glyphicon glyphicon-home"></span>Home</a>
        </li>
        <li>
            <a href="{{ route('admin.user.list') }}"><span class="glyphicon glyphicon-user"></span>User</a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-music"></span>Music<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li class="dropdown-header">Dropdown heading</li>
            <li><a href="#">Thêm Bài Hát</a></li>
            <li><a href="#">Quản Lý Album</a></li>
            <li><a href="#">Thêm Album</a></li>
            <li><a href="#">Quản Lý Ca Sĩ</a></li>
            <li><a href="#">Thêm Một Ca Sĩ</a></li>
            <li><a href="#">Quản Lý Nhạc Sĩ</a></li>
            <li><a href="#">Thêm Nhạc Sĩ</a></li>
            <li><a href="#">Quản Lý Bình Luận
            </a></li>
            <li><a href="#">Quản Lý Playlist</a></li>
            <li><a href="#">Quản Lý Thể Loại Bài Hát</a></li>
            <li><a href="#">Quản Lý Quốc Gia</a></li>
          </ul>
        </li>
        <li>
            <a href="#"><span class="glyphicon glyphicon-envelope"></span>Liên Hệ</a>
        </li>
    </ul>
</nav>