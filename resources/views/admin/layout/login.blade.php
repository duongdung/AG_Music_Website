<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login admin</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- MY CSS -->
	<link  href="{!!asset('admin/css/style_login.css') !!}" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Sign in to admin MP3</h1>
            <div class="col-lg-12 ">
            	@if(session('thongbao'))
            	<div class="alert alert-danger">
            		{{session('thongbao')}}
            	</div>
            	@endif
            </div>
            <div class="account-wall">
                <img class="profile-img" src="{{ asset('admin/img/photo_login.png')}}"
                    alt="photo_login">
                <form class="form-signin" method="POST" action="{{ route('admin.postLogin') }}">
                	<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
	                <input type="text" class="form-control" name="name" placeholder="User Name" required autofocus>
	                <input type="password" class="form-control" name="password" placeholder="Password" required>
	                <button class="btn btn-lg btn-primary btn-block" type="submit">
	                    Sign in</button>
	                <label class="checkbox pull-left">
	                    <input type="checkbox" value="remember-me">
	                    Remember me
	                </label>
	                <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                </form>
            </div>
        </div>
    </div>
</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>