<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <meta name="description" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{!!asset('css/bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!!asset('css/normalize.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!!asset('css/main.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!!asset('css/app.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!!asset('css/font-awesome.min.css') !!}">
</head>
<body>
<div class="main">
    <!--Start navbar-->
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded main-nav">

        <!--Nút menu giao diện điện thoại-->
        <button class="navbar-toggler navbar-toggler-right button-menu" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation">
            <i class="icon-menu fa fa-bars" aria-hidden="true"></i>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}">LOGO</a>

        <!--Menu chính-->
        <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item">
                    <a class="nav-link nav-border-right" href="#">
                        <i class="fa fa-headphones" aria-hidden="true"></i>
                        <p>Trang Chủ</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="fa fa-music" aria-hidden="true"></i>
                        <p>Nhạc Cá Nhân</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-border-right" href="#">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>
                        <p>Thể Loại</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="fa fa-microphone" aria-hidden="true"></i>
                        <p>Ca Sỹ</p>
                    </a>
                </li>
                @if (Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link nav-border-right" href="{{ route('login' )}}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            <p>Đăng nhập</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            <p>Đăng ký</p>
                        </a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link nav-border-right" href="#">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            <p>{{ Auth::user()->name }}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            <p>Logout</p>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </li>
                @endif
            </ul>
        </div>
        <!--Hết menu chính-->
    </nav>
    <!--Hết navbar-->

    <div class="backgroup-black"></div>
    @yield('content')
</div>
<!--Hết main-->
<!-- Scripts -->
<script src="{!!asset('js/vendor/jquery-1.12.0.min.js')!!}"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="{!!asset('js/vendor/bootstrap.js')!!}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ScrollToPlugin.min.js"></script>
<script src="{!!asset('js/jaudio.js')!!}"></script>
<script src="{!!asset('js/player.js')!!}"></script>
<script type="text/javascript" src="{!!asset('js/modernizr.custom.53451.js')!!}"></script>
<script type="text/javascript" src="{!!asset('js/jquery.gallery.js')!!}"></script>
<script src="{!!asset('js/my-js.js')!!}"></script>
<script src="{!!asset('js/plugins.js')!!}"></script>
<script type="text/javascript">
    $(function () {
        $('#dg-container').gallery({
            autoplay: true
        });
    });
</script>
</body>
</html>
