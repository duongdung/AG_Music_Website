@extends('layouts.app')
@section('content')
    <!--Start content 1-->
    
    <div class="content-first">
        <div class="container">
            <div class="row">

                {{--Bắt đầu phần chữ trên đầu trang--}}
                <div class="quotes col-12">
                    <div class="quotes-bg"></div>
                    <div class="qoutes-bg-color"></div>
                    <div class="quotes-content">
                        <h1>M<span>U</span>SIC</h1>
                        <h3>Make me <span>happy</span></h3>
                    </div>
                    <div class="quotes-btn">
                        <button type="button" class="btn-head btn btn-my-music">MY MUSIC</button>
                        <button type="button" class="btn-head btn btn-theloai">THỂ LOẠI</button>
                    </div>
                </div>
                {{--Hết phần chữ đầu trang--}}

                <!--Bắt đầu bảng xếp hạng-->
                {{--<div class="ranking col-12">--}}
                    <div class="player">

                        <div class="media-wrapper">
                            <div class='music-card playing jAudio'>
                                <audio id="myAudio"></audio>

                                {{--refactor--}}
                                {{--Phần hiển thị thông tin nhạc--}}
                                <div class="jAudio--ui">
                                    <div class="button-list">
                                        <i class="fa fa-list-ul" aria-hidden="true"></i>
                                    </div>
                                    <div class='image jAudio--thumb'></div>
                                    <div class="jAudio--status-bar">
                                        <div class='info jAudio--details'></div>
                                        <div class="audio-time jAudio--progress-bar">
                                            <div class="time current-time">0:00</div>
                                            <div class="progress"><span class="progress-span"></span></div>
                                            <div class="time duration">0:00</div>
                                        </div>
                                    </div>
                                </div>

                                {{--Các nút điều khiển trên player--}}
                                <div class='jAudio--controls player-controls'>
                                    <div class="button-control trigger btn-volume">
                                        <i class="fa fa-volume-up arrow volume-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class='jAudio--control jAudio--control-prev button-control trigger'
                                         data-action='prev'>
                                        <i class="fa fa-backward arrow" style="left: 46%" aria-hidden="true"></i>
                                    </div>
                                    <div class='jAudio--control jAudio--control-play button-control trigger play-pause'
                                         data-action='play'>
                                        <i class="fa fa-play arrow arrow-play" aria-hidden="true"></i>
                                    </div>
                                    <div class='jAudio--control jAudio--control-next button-control trigger'
                                         data-action='next'>
                                        <i class="fa fa-forward arrow" style="left: 55%" aria-hidden="true"></i>
                                    </div>
                                    <div class="button-control trigger jAudio--download">
                                        <a class="button-download" href="" download><i class="fa fa-cloud-download arrow" aria-hidden="true"></i></a>
                                    </div>
                                </div>

                                {{--Hiệu ứng lượn sóng trên player--}}
                                <div class="wave-animation">
                                    <div class='wave'></div>
                                    <div class='wave'></div>
                                    <div class='wave'></div>
                                </div>

                                {{--Phần playlist trên player--}}
                                <div class='jAudio--playlist'></div>

                            </div>
                        </div>
                    </div>
                {{--</div>--}}
                <!--Hết bảng player-->

            </div>
            <!--Het row-->
        </div>
        <!--Hết container-->
    </div>

    <!--Start slideshow-->
    <div class="slideshow">
        <div class="container">
            <div class="row">
                <div class="dg-main">
                    <p class="slideshow-title">Album Hot</p>
                    <section id="dg-container" class="dg-container">
                        <div class="dg-wrapper">
                            @foreach($slideShow as $image)
                                <a href="#">
                                    <img class="img-fluid img-responsive" src="{{ asset('storage/upload/images/albums/'.$image->img) }}" alt="image01">
                                </a>
                            @endforeach
                        </div>
                        <div class="btn-slideshow">
                            <div class="btn-prev"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>
                            <div class="btn-next"><i class="fa fa-arrow-right" aria-hidden="true"></i></div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <!--Start User-->
    @if(Auth::guest())
    @else
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="user-background"></div>
                <div class="user-hello col-12">
                    <p>Hi!</p>
                    <span>{{ Auth::user()->name}}</span>
                </div>
                <div class="user-play col-12">
                    <div class="user-play-img">
                        <i class="fa fa-play" aria-hidden="true"></i>
                        <img src="{{ asset('storage/upload/images/imguser/'.Auth::user()->image) }}" class="img-user-home">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!--Start The loai-->
    @include('user.template.theloainhac')

    <!--Start Ca Sy-->
    @include('user.template.singer')
@endsection