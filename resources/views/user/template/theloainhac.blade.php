<div class="categories">
    <div class="container">
        <div class="row">
            @foreach($categoryList as $country)
                <div class="category col-12">
                    <a href="#">
                        <h5>Nhạc {{ $country->name}}</h5>
                        <i class="category-icon fa fa-hand-o-left" aria-hidden="true"></i>
                    </a>
                    <div class="category-list">
                        @foreach($country->categories as $category)
                            <a class="list-img">
                                <img src="{!! asset('storage/upload/images/category/'.$category->image) !!}" class="img-responsive"
                                     alt="">
                                <div class="list-title">
                                    <p class="cate_name">{{ $category->name }}</p>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>