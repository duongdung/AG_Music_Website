<div class="singers">
    <div class="container">
        <div class="row">
            @foreach($singerList as $country)
                <div class="singer col-12">
                    <a href="">
                        <h5>Ca Sỹ {{ $country->name }}</h5>
                        <i class="category-icon fa fa-hand-o-left" aria-hidden="true"></i>
                    </a>
                    <div class="singer-list">
                        @foreach($country->singers as $singer)
                            @if($singer->hot > 0)
                            <a class="list-img">
                                <img src="{{asset('storage/upload/images/singers/'.$singer->img)}}" class="img-responsive" alt="">
                                <div class="list-title">
                                    <p>{!! $singer->name !!}</p>
                                </div>
                            </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>